# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.5.1 - Update docs
### Changed
- API route 

## 0.5.0 - Request Logging
### Added
- /set_success API route
- /set_fail API route
- Request logging

### Changed
- Database schema

### Fixed
- Usage graph

## [0.4.0] - Activity Log
### Added
- Activity log
- SQLite database

## [0.3.0] - Image tagging
### Added
- Image tagging
- Configuration file

## [0.2.0] - OCR
### Added
- Optical Character Recognition

## [0.1.0] - Web interface
### Added
- Web interface
- Command line application
- RESTful API

[0.4.0]: https://bitbucket.org/bkvaluemeal/d3cryp7.py/issues/4/activity-log
[0.3.0]: https://bitbucket.org/bkvaluemeal/d3cryp7.py/issues/3/image-tagging
[0.2.0]: https://bitbucket.org/bkvaluemeal/d3cryp7.py/issues/2/ocr
[0.1.0]: https://bitbucket.org/bkvaluemeal/d3cryp7.py/issues/1/web-interface
